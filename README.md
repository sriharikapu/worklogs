# WorkLogs

![](https://gitlab.com/sriharikapu/worklogs/-/raw/master/images/snippet.PNG)

### sample LOG format for updating logs

```
[   {
        ...... Old logs remain here # Cronology is based on Date
    },
    {
		"ORG": "Organization Name",
		"TIMESTAMP": "25-08-2020",
		"AUTHOR": "Srihari Kapu",
		"DURATION": "2hrs 30 min",
		"TASKS": [{
				"ACCOMPLISHED": ["Task1", "Task2"],
				"DETAILS": ["Meaningful short decription"]
			},
			{
				"TODO": ["Tasks set to funish next"]
			},
			{
				"BACKLOGS": "Pending tasks"
			}
		]
	},{
        ...... New Log Goes here
    }
]

```

### How it works?

Append your log to the `logs/logs.json` file and let the magic happen on the live website below.

### view live website

[click here](https://sriharikapu.com/elogs/#)

### API KEY FOR EMAILS

[smtp2go](https://app.smtp2go.com/)

