var J2HConverter = function(sourceJson, container) { 
    this.sourceJson = sourceJson.trim();
    this.container = container;
    this.attributes = null;
}

J2HConverter.prototype = {

    convert: function() {
        var jsonStr = this._jsonObjToArray(this.sourceJson);
        var data = JSON.parse(jsonStr);
        var table = this._createTable(data);
        if (table) {
            this._setAttributes(table, this.attributes);
            document.querySelector('#' + this.container).innerHTML = '';
            document.querySelector('#' + this.container).appendChild(table);
        }
    },

    _createTable: function(jsonData) {
        var jsonStruct = this._getJsonStructure(jsonData);
        if (jsonStruct == this._jsonStruct.unknown)
            throw 'JSON structure not supported. Root object must be an array.';

        var keys = [];
        if (jsonStruct == this._jsonStruct.stringArray) keys.push('item');
        else if (jsonStruct == this._jsonStruct.objectArray) keys = this._getKeys(jsonData);

        if (keys.length > 0) {
            var table = document.createElement('table');
            this._setHeader(table, keys);
            this._setBody(table, keys, jsonData, jsonStruct);
            return table;
        }
        return null;
    },

    _getJsonStructure: function(parsedJson) {
        if(parsedJson && parsedJson.length > 0) {
            if (typeof parsedJson[0] == 'string') return this._jsonStruct.stringArray;
            else if (typeof parsedJson[0] == 'object') return this._jsonStruct.objectArray;
        }
        return this._jsonStruct.unknown;
    },

    _setAttributes: function(srcTable, attributes) {
        if (attributes) {
            for(var key in attributes)
                srcTable.setAttribute(key, attributes[key]);
        }
        else {
            srcTable.border = '1';
            srcTable.style.borderCollapse = 'collapse';
        }
    },

    _setHeader: function(srcTable, colData) {
        var header = srcTable.createTHead();
        var headerRow = header.insertRow(0);
        colData.forEach(function(colDataItem, colDataItemIdx) {
            var cell = headerRow.insertCell(colDataItemIdx);
            cell.textContent = colDataItem;
        });
    },

    _setBody: function(srcTable, colData, rowData, jsonStruct) {
        var body = srcTable.createTBody();
        var row, cell;
        rowData.forEach(function(rowDataItem, rowDataItemIdx) {
            row = body.insertRow(rowDataItemIdx);
            colData.forEach(function(colDataItem, colDataItemIdx) {
                cell = row.insertCell(colDataItemIdx);
                if (jsonStruct == this._jsonStruct.stringArray)
                    cell.textContent = rowDataItem;
                else if (jsonStruct == this._jsonStruct.objectArray) {
                    if(rowDataItem.hasOwnProperty(colDataItem)) {
                        var colDataStr = this._jsonObjToArray(JSON.stringify(rowDataItem[colDataItem]));
                        var colDataJson = JSON.parse(colDataStr);
                        var colDataStruct = this._getJsonStructure(colDataJson);
                        if (colDataStruct == this._jsonStruct.unknown || colDataStruct == this._jsonStruct.stringArray) {
                            cell.textContent = colDataJson; // Show as-is
                        }
                        else {
                            var info = document.createElement('a');
                            info.innerHTML = '&#43;';
                            info.href = '#';
                            info.style.fontWeight = 'bold';
                            info.style.textDecoration = 'none';
                            info.args = {
                                srcTable: srcTable,
                                srcColumn: colDataItem,
                                rowData: colDataJson,
                                processRowData: true,
                                colState: 'collapsed'
                            };
                            info.addEventListener('click', this._toggleDetail.bind(this), false);
                            cell.appendChild(info);
                        }
                    }
                }
            }, this);
        }, this);
    },

    _toggleDetail: function(e) {
        var srcTable = e.target.args.srcTable;
        var srcColumn = e.target.args.srcColumn;
        var rowData = e.target.args.rowData;
        var parentRowIdx, trHeading, trDetail;

        if(e.target.args.colState == 'collapsed') {
            if(e.target.args.processRowData) { // Create table only for the first time
                parentRowIdx = e.target.parentElement.parentElement.rowIndex;

                trHeading = srcTable.tBodies[0].insertRow(parentRowIdx);
                trHeading.id = 'trh_' + parentRowIdx + '_' + srcColumn;
                var tdHeading = trHeading.insertCell(0);
                tdHeading.colSpan = srcTable.rows[parentRowIdx].cells.length.toString();
                tdHeading.style.fontWeight = 'bold';
                tdHeading.textContent = srcColumn;

                trDetail = srcTable.tBodies[0].insertRow(parentRowIdx + 1);
                trDetail.id = 'trd_' + parentRowIdx + '_' + srcColumn;
                var tdDetail = trDetail.insertCell(0);
                tdDetail.colSpan = srcTable.rows[parentRowIdx].cells.length.toString();

                var table = this._createTable(rowData);
                if (table) tdDetail.appendChild(table);

                e.target.args.processRowData = false;
                e.target.args.parentRowIdx = parentRowIdx;
            }
            else { // Just display on subsequent toggles
                parentRowIdx = e.target.args.parentRowIdx;
                trHeading = srcTable.tBodies[0].querySelector('#trh_' + parentRowIdx + '_' + srcColumn);
                trDetail = srcTable.tBodies[0].querySelector('#trd_' + parentRowIdx + '_' + srcColumn);
                trHeading.style.display = 'table-row';
                trDetail.style.display = 'table-row';
            }
            e.target.args.colState = 'expanded';
            e.target.innerHTML = '&ndash;';
        }
        else if (e.target.args.colState == 'expanded') { // Hide
            parentRowIdx = e.target.args.parentRowIdx;
            trHeading = srcTable.tBodies[0].querySelector('#trh_' + parentRowIdx + '_' + srcColumn);
            trDetail = srcTable.tBodies[0].querySelector('#trd_' + parentRowIdx + '_' + srcColumn);
            trHeading.style.display = 'none';
            trDetail.style.display = 'none';
            e.target.args.colState = 'collapsed';
            e.target.innerHTML = '&#43;';
        }
    },

    _getKeys: function(json) {
    	var keys = [];
    	json.forEach(function(item) {
        	for(var key in item) {
        		if(keys.indexOf(key) > -1)
        			continue;
        		keys.push(key);
        	}
        });
        return keys;
    },

    _jsonObjToArray: function(jsonStr) {
        if (jsonStr.startsWith('{') && jsonStr.endsWith('}'))
            return '[' + jsonStr + ']'; // Change to array of single object
        else
            return jsonStr;
    },

    _jsonStruct: {
        stringArray: 'JSON_AS_STRING_ARRAY',
        objectArray: 'JSON_AS_OBJECT_ARRAY',
        unknown: 'JSON_STRUCTURE_UNKNOWN'
    }
}

////////////////////

async function shot() {
                    let url = 'https://gitlab.com/sriharikapu/worklogs/-/raw/master/logs/logs.json';
                    let proxyUrl = 'https://cors-anywhere.herokuapp.com/';
                    let obj = await (await fetch(proxyUrl+url)).json();
                    let txt = JSON.stringify(obj);
                    //console.log(obj);
                    var j2ht = new J2HConverter(txt, 'demo');
                    j2ht.attributes = { class: 'j2ht_table', cellspacing: '0', cellpadding: '0' };
                    j2ht.convert();
                }

            window.addEventListener('load', function() {
                var btnConvert = document.querySelector('#btnConvert1');
                btnConvert.addEventListener('click', shot());
});

async function sendMail() {
            var url = "https://api.smtp2go.com/v3/email/send";
            var to = document.getElementById('txtJson').value;
            let JSONurl = 'https://gitlab.com/sriharikapu/worklogs/-/raw/master/logs/logs.json';
            let proxyUrl = 'https://cors-anywhere.herokuapp.com/';
            let obj = await (await fetch(proxyUrl+JSONurl)).json();
            let text = JSON.stringify(obj);

            var items=[];
            var tasks=[];
            var accomp=[];


            for (var key in obj) {
                 if (obj.hasOwnProperty(key)) {
                    var val = obj[key];
                }
            }

            for (var key in val) {
                if (val.hasOwnProperty(key)) {
                    var value1 = val[key];
                }
                items.push(value1);
            }

            var set = items[4];

            for (var key in set) {
                if (set.hasOwnProperty(key)) {
                    var value2 = set[key];
                }
                tasks.push(value2);
            }

            var acp = set[0];

            for (var key in acp) {
                if (acp.hasOwnProperty(key)) {
                    var value3 = acp[key];
                }
                accomp.push(value3);
            }

            $.ajax({
                    url: url,
                    method: 'POST',
                    headers: {
                    'Content-Type': "application/json"
                    },
                    data: JSON.stringify({
                        'sender': "\"Srihari Kapu\" <sri@sriharikapu.com>",
                        'to': [to],
                        'api_key': "api-KEY",
                        'subject': "Engineering Logs",
                        'html_body': "<h1> Latest Logs : </h1>"
                                     +"<p>_________________________</p>"
                                     +"<b> ORG NAME : </b> "+items[0]
                                     +"<br> <b> DATE : </b> "+items[1]
                                     +"<br> <b> AUTHOR : </b> "+items[2]
                                     +"<br> <b> DURATION : </b> "+items[3]
                                     +"<br> <b> TASKS ACCOMPLISHED : </b><br> 1. "+accomp[0][0]+"<br> 2. "+accomp[0][1]
                                     +"<br> <b> DESCRIPTION : </b><br> "+ accomp[1][1],
                        'text_body': "Hello There 👋🏼👋🏼, Here is the recent longs!"
                    }),
                }).done(function(result) {
                    //console.log(result);
                    alert("Mail Delivered!!");
                }).fail(function(err) {
                throw err;
            });
}