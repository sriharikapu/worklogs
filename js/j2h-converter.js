var J2HConverter = function(t, e) {
    this.sourceJson = t.trim(), this.container = e, this.attributes = null
};
J2HConverter.prototype = {
    convert: function() {
        var t = this._jsonObjToArray(this.sourceJson),
            e = JSON.parse(t),
            r = this._createTable(e);
        r && (this._setAttributes(r, this.attributes), document.querySelector("#" + this.container).innerHTML = "", document.querySelector("#" + this.container).appendChild(r))
    },
    _createTable: function(t) {
        var e = this._getJsonStructure(t);
        if (e == this._jsonStruct.unknown) throw "JSON structure not supported. Root object must be an array.";
        var r = [];
        if (e == this._jsonStruct.stringArray ? r.push("item") : e == this._jsonStruct.objectArray && (r = this._getKeys(t)), r.length > 0) {
            var n = document.createElement("table");
            return this._setHeader(n, r), this._setBody(n, r, t, e), n
        }
        return null
    },
    _getJsonStructure: function(t) {
        if (t && t.length > 0) {
            if ("string" == typeof t[0]) return this._jsonStruct.stringArray;
            if ("object" == typeof t[0]) return this._jsonStruct.objectArray
        }
        return this._jsonStruct.unknown
    },
    _setAttributes: function(t, e) {
        if (e)
            for (var r in e) t.setAttribute(r, e[r]);
        else t.border = "1", t.style.borderCollapse = "collapse"
    },
    _setHeader: function(t, e) {
        var r = t.createTHead().insertRow(0);
        e.forEach(function(t, e) {
            r.insertCell(e).textContent = t
        })
    },
    _setBody: function(t, e, r, n) {
        var s, o, a = t.createTBody();
        r.forEach(function(r, i) {
            s = a.insertRow(i), e.forEach(function(e, a) {
                if (o = s.insertCell(a), n == this._jsonStruct.stringArray) o.textContent = r;
                else if (n == this._jsonStruct.objectArray && r.hasOwnProperty(e)) {
                    var i = this._jsonObjToArray(JSON.stringify(r[e])),
                        c = JSON.parse(i),
                        l = this._getJsonStructure(c);
                    if (l == this._jsonStruct.unknown || l == this._jsonStruct.stringArray) o.textContent = c;
                    else {
                        var u = document.createElement("a");
                        u.innerHTML = "&#43;", u.href = "#", u.style.fontWeight = "bold", u.style.textDecoration = "none", u.args = {
                            srcTable: t,
                            srcColumn: e,
                            rowData: c,
                            processRowData: !0,
                            colState: "collapsed"
                        }, u.addEventListener("click", this._toggleDetail.bind(this), !1), o.appendChild(u)
                    }
                }
            }, this)
        }, this)
    },
    _toggleDetail: function(t) {
        var e, r, n, s = t.target.args.srcTable,
            o = t.target.args.srcColumn,
            a = t.target.args.rowData;
        if ("collapsed" == t.target.args.colState) {
            if (t.target.args.processRowData) {
                e = t.target.parentElement.parentElement.rowIndex, (r = s.tBodies[0].insertRow(e)).id = "trh_" + e + "_" + o;
                var i = r.insertCell(0);
                i.colSpan = s.rows[e].cells.length.toString(), i.style.fontWeight = "bold", i.textContent = o, (n = s.tBodies[0].insertRow(e + 1)).id = "trd_" + e + "_" + o;
                var c = n.insertCell(0);
                c.colSpan = s.rows[e].cells.length.toString();
                var l = this._createTable(a);
                l && c.appendChild(l), t.target.args.processRowData = !1, t.target.args.parentRowIdx = e
            } else e = t.target.args.parentRowIdx, r = s.tBodies[0].querySelector("#trh_" + e + "_" + o), n = s.tBodies[0].querySelector("#trd_" + e + "_" + o), r.style.display = "table-row", n.style.display = "table-row";
            t.target.args.colState = "expanded", t.target.innerHTML = "&ndash;"
        } else "expanded" == t.target.args.colState && (e = t.target.args.parentRowIdx, r = s.tBodies[0].querySelector("#trh_" + e + "_" + o), n = s.tBodies[0].querySelector("#trd_" + e + "_" + o), r.style.display = "none", n.style.display = "none", t.target.args.colState = "collapsed", t.target.innerHTML = "&#43;")
    },
    _getKeys: function(t) {
        var e = [];
        return t.forEach(function(t) {
            for (var r in t) e.indexOf(r) > -1 || e.push(r)
        }), e
    },
    _jsonObjToArray: function(t) {
        return t.startsWith("{") && t.endsWith("}") ? "[" + t + "]" : t
    },
    _jsonStruct: {
        stringArray: "JSON_AS_STRING_ARRAY",
        objectArray: "JSON_AS_OBJECT_ARRAY",
        unknown: "JSON_STRUCTURE_UNKNOWN"
    }
};
